db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "23154565",
            email: "stephenhawk@gmail.com"
        },
        courses: ["Astrodynamics", "Cosmotologist", "Author", "English Theoretical Physicist"],
        department: "HR"
    },
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "123654987",
            email: "janedoe@gmail.com"
        },
        courses: ["English", "Maths", "Sciences"],
        department: "HR"
    },
    {
        firstName: "Ivan",
        lastName: "Cura",
        age: 24,
        contact: {
            phone: "12301985",
            email: "ivancura9485@gmail.com"
        },
        courses: ["HTML", "MongoDB", "JavaScript"],
        department: "IT"
    },
    {
        firstName: "Michael",
        lastName: "Bay",
        age: 35,
        contact: {
            phone: "120398045",
            email: "michaelfilms@gmail.com"
        },
        courses: ["Film", "Art", "Design"],
        department: "Marketing"
    }
])

// 2. Find users with letter s in their first name or d in their last name.
// a. Use the $or operator.
// b. Show only the firstName and lastName fields and hide the _id field.

db.users.find({
    $or: [
        { firstName: { $regex: "s", $options: "$i" } },
        { lastName: { $regex: "d", $options: "$i" } }
    ]
}, { firstName: 1, lastName: 1, _id: 0 }
)

// 3. Find users who are from the HR department and their age is greater
// than or equal to 70.
// a. Use the $and operator

db.users.find({
    $and: [
        { department: "HR" },
        { age: { $gte: 70 } }
    ]
})

// 4. Find users with the letter e in their first name and has an age of less
// than or equal to 30.
// a. Use the $and, $regex and $lte operators

db.users.find({
    $and: [
        { firstName: { $regex: "e", $options: "$i" } },
        { age: { $lte: 30 } }
    ]
})